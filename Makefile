# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.0

# Default target executed when no arguments are given to make.
default_target: all
.PHONY : default_target

# Allow only one "make -f Makefile2" at a time, but pass parallelism.
.NOTPARALLEL:
.PHONY : .NOTPARALLEL

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/gmarciani/Workspace/cimple

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/gmarciani/Workspace/cimple

#=============================================================================
# Targets provided globally by CMake.

# Special rule for the target edit_cache
edit_cache:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "No interactive CMake dialog available..."
	/usr/bin/cmake -E echo No\ interactive\ CMake\ dialog\ available.
.PHONY : edit_cache

# Special rule for the target edit_cache
edit_cache/fast: edit_cache
.PHONY : edit_cache/fast

# Special rule for the target rebuild_cache
rebuild_cache:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Running CMake to regenerate build system..."
	/usr/bin/cmake -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR)
.PHONY : rebuild_cache

# Special rule for the target rebuild_cache
rebuild_cache/fast: rebuild_cache
.PHONY : rebuild_cache/fast

# The main all target
all: cmake_check_build_system
	$(CMAKE_COMMAND) -E cmake_progress_start /home/gmarciani/Workspace/cimple/CMakeFiles /home/gmarciani/Workspace/cimple/CMakeFiles/progress.marks
	$(MAKE) -f CMakeFiles/Makefile2 all
	$(CMAKE_COMMAND) -E cmake_progress_start /home/gmarciani/Workspace/cimple/CMakeFiles 0
.PHONY : all

# The main clean target
clean:
	$(MAKE) -f CMakeFiles/Makefile2 clean
.PHONY : clean

# The main clean target
clean/fast: clean
.PHONY : clean/fast

# Prepare targets for installation.
preinstall: all
	$(MAKE) -f CMakeFiles/Makefile2 preinstall
.PHONY : preinstall

# Prepare targets for installation.
preinstall/fast:
	$(MAKE) -f CMakeFiles/Makefile2 preinstall
.PHONY : preinstall/fast

# clear depends
depend:
	$(CMAKE_COMMAND) -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR) --check-build-system CMakeFiles/Makefile.cmake 1
.PHONY : depend

#=============================================================================
# Target rules for targets named hello

# Build rule for target.
hello: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 hello
.PHONY : hello

# fast build rule for target.
hello/fast:
	$(MAKE) -f CMakeFiles/hello.dir/build.make CMakeFiles/hello.dir/build
.PHONY : hello/fast

#=============================================================================
# Target rules for targets named vecadd

# Build rule for target.
vecadd: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 vecadd
.PHONY : vecadd

# fast build rule for target.
vecadd/fast:
	$(MAKE) -f CMakeFiles/vecadd.dir/build.make CMakeFiles/vecadd.dir/build
.PHONY : vecadd/fast

src/basic/hello/hello.o: src/basic/hello/hello.c.o
.PHONY : src/basic/hello/hello.o

# target to build an object file
src/basic/hello/hello.c.o:
	$(MAKE) -f CMakeFiles/hello.dir/build.make CMakeFiles/hello.dir/src/basic/hello/hello.c.o
.PHONY : src/basic/hello/hello.c.o

src/basic/hello/hello.i: src/basic/hello/hello.c.i
.PHONY : src/basic/hello/hello.i

# target to preprocess a source file
src/basic/hello/hello.c.i:
	$(MAKE) -f CMakeFiles/hello.dir/build.make CMakeFiles/hello.dir/src/basic/hello/hello.c.i
.PHONY : src/basic/hello/hello.c.i

src/basic/hello/hello.s: src/basic/hello/hello.c.s
.PHONY : src/basic/hello/hello.s

# target to generate assembly for a file
src/basic/hello/hello.c.s:
	$(MAKE) -f CMakeFiles/hello.dir/build.make CMakeFiles/hello.dir/src/basic/hello/hello.c.s
.PHONY : src/basic/hello/hello.c.s

src/basic/vecadd/vecadd.o: src/basic/vecadd/vecadd.c.o
.PHONY : src/basic/vecadd/vecadd.o

# target to build an object file
src/basic/vecadd/vecadd.c.o:
	$(MAKE) -f CMakeFiles/vecadd.dir/build.make CMakeFiles/vecadd.dir/src/basic/vecadd/vecadd.c.o
.PHONY : src/basic/vecadd/vecadd.c.o

src/basic/vecadd/vecadd.i: src/basic/vecadd/vecadd.c.i
.PHONY : src/basic/vecadd/vecadd.i

# target to preprocess a source file
src/basic/vecadd/vecadd.c.i:
	$(MAKE) -f CMakeFiles/vecadd.dir/build.make CMakeFiles/vecadd.dir/src/basic/vecadd/vecadd.c.i
.PHONY : src/basic/vecadd/vecadd.c.i

src/basic/vecadd/vecadd.s: src/basic/vecadd/vecadd.c.s
.PHONY : src/basic/vecadd/vecadd.s

# target to generate assembly for a file
src/basic/vecadd/vecadd.c.s:
	$(MAKE) -f CMakeFiles/vecadd.dir/build.make CMakeFiles/vecadd.dir/src/basic/vecadd/vecadd.c.s
.PHONY : src/basic/vecadd/vecadd.c.s

# Help Target
help:
	@echo "The following are some of the valid targets for this Makefile:"
	@echo "... all (the default if no target is provided)"
	@echo "... clean"
	@echo "... depend"
	@echo "... edit_cache"
	@echo "... hello"
	@echo "... rebuild_cache"
	@echo "... vecadd"
	@echo "... src/basic/hello/hello.o"
	@echo "... src/basic/hello/hello.i"
	@echo "... src/basic/hello/hello.s"
	@echo "... src/basic/vecadd/vecadd.o"
	@echo "... src/basic/vecadd/vecadd.i"
	@echo "... src/basic/vecadd/vecadd.s"
.PHONY : help



#=============================================================================
# Special targets to cleanup operation of make.

# Special rule to run CMake to check the build system integrity.
# No rule that depends on this can have commands that come from listfiles
# because they might be regenerated.
cmake_check_build_system:
	$(CMAKE_COMMAND) -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR) --check-build-system CMakeFiles/Makefile.cmake 0
.PHONY : cmake_check_build_system

