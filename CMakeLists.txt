cmake_minimum_required(VERSION 3.0)

project(cimple)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

set(HELLO_SOURCE_FILES src/basic/hello/hello.c)
set(VECADD_SOURCE_FILES src/basic/vecadd/vecadd.c)

add_executable(hello ${HELLO_SOURCE_FILES})
add_executable(vecadd ${VECADD_SOURCE_FILES})