/*******************************************************************************
* PRINT
*
* @description: Simple example on using OpenMPI to make parallel prints.
* @date: 14 October 2016
* @author: Giacomo Marciani <gmarciani@acm.org>
*******************************************************************************/

#include <omp.h>
#include <stdio.h>

int main() {
	printf("Sequential section\n");

	#pragma omp parallel
	{
		printf("Parallel section %d\n", omp_get_thread_num());
	}

	printf("Sequential section\n");
	return 0;
}
