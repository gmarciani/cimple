/*******************************************************************************
* @Program: hello.c*
* @Description: The classic `Hello World`.
*
* @Author: Giacomo Marciani <gmarciani@acm.org>
* @Institution: University of Rome Tor Vergata
*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>

int main(void) {
    printf("Hello world!\n");
    exit(0);
}
