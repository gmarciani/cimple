/*******************************************************************************
* @Program: vecadd.c
* @Description: Vectorial addition.
*
* @Author: Giacomo Marciani <gmarciani@acm.org>
* @Institution: University of Rome Tor Vergata
*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

int VECTOR_DIMENSION = 10;
double MAX = 100;

void vectorRnd(double A[]) {
  int i;
  for (i = 0; i < VECTOR_DIMENSION; i++) {
    A[i] = (double)rand()/(double)RAND_MAX * MAX;
  }
}

void vectorAdd(double A[], double B[], double C[]) {
  int i;
  for (i = 0; i < VECTOR_DIMENSION; i++) {
    C[i] = A[i] + B[i];
  }
}

void vectorPrint(char vectorName[], double A[]) {
  printf("%s=[", vectorName);
  int i;
  for (i = 0; i < VECTOR_DIMENSION; i++) {
    printf("%lf%c", A[i], (i < VECTOR_DIMENSION - 1) ? ',': '\b');
  }
  printf("]\n");
}

int main(int argc, char *argv[]) {
  double A[VECTOR_DIMENSION];
  double B[VECTOR_DIMENSION];
  double C[VECTOR_DIMENSION];

  srand((unsigned) time(NULL));

  vectorRnd(A);
  vectorRnd(B);
  vectorAdd(A, B, C);

  vectorPrint("A", A);
  vectorPrint("B", B);
  vectorPrint("C", C);

  exit(0);
}


